/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lgs.simplestring;

import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;

/**
 *
 * @author matteo
 */
public class Producer {

	final static Logger loggo = Logger.getLogger(Producer.class);
	
	public static void main(String[] args) {
		
		try {
		
			// TODO Auto-generated method stub
			Properties props = new Properties();
			//props.put("bootstrap.servers","10.0.2.50:9092");
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"10.0.2.50:9092");
			//props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
			//props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
			
			KafkaProducer<String, String> producer = new KafkaProducer<>(props);
			
			for (int i = 0; i<100; i++){
				
				ProducerRecord<String, String> record = new ProducerRecord<String, String>("mytopic1", "MER-DA-DERET'NO- " + i + "xx909");
				producer.send(record);
			}
			
			loggo.info("==========DONE!!========");
			producer.close();
		} catch (Exception e) {
			loggo.info("errore: " + e.getMessage());
		}
	}

}

