/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lgs.simplestring;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author matteo
 */
public class HubAllProducer {

	final static Logger loggo = Logger.getLogger(HubAllProducer.class);
	static final long serialVersionUID = 3834740225030075752L;
	
	
	
	public static void main(String[] args) {
		
		try {
		
			// TODO Auto-generated method stub
			
			Map<String, String> dispositiviTopics = new HashMap<String, String>(){
				{
					put(new String("bra"),new String("braciale"));
					put(new String("mac"),new String("macchina"));
					put(new String("tra"),new String("trattore"));
					put(new String("mie"),new String("mietitrebbia"));
					put(new String("ani"),new String("animale"));
					put(new String("ana"),new String("anal_intruder"));
				};
			};
			String[] metriche = {"0_74_speed", "0_75_pression", "0_76_giri", "0_77_temp", "0_78_cal"};
			
			Properties props = new Properties();
			//props.put("bootstrap.servers","10.0.2.50:9092");
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"10.0.2.50:9092");
			//props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
			//props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
			KafkaProducer<String, String> producer = new KafkaProducer<>(props);
			int numTopics = dispositiviTopics.size();
			int numMetriche = metriche.length;
			SimpleDateFormat sf = new SimpleDateFormat();
			sf.applyPattern("yyyyMMddHHmmss");
			for(int od=0;od<20;od++){
				for(int o=0;o<100;o++){
					long numeroCasuale = (System.currentTimeMillis());
					String chiave = dispositiviTopics.keySet().toArray()[(int) (numeroCasuale%numTopics)].toString();
					
					String idNodo =chiave;
					idNodo += ""+System.currentTimeMillis()%1234567;
					
					String topic = dispositiviTopics.get(chiave);
					Date dataMetriche;
					Map<String,Float> qlfAndValue;
					
					String dataStr = sf.format(new Date());
					
					JSONObject invioJSON = new JSONObject();
					invioJSON.put("node_desc", idNodo);
					invioJSON.put("dateTime", dataStr );
					int iter = Math.max(3, new Random().nextInt());
					JSONArray metrics = new JSONArray();
					for (int i = 0; i<metriche.length ;i++ ){
						metrics.add(metriche[i] + ": " +System.currentTimeMillis()%(10+i));
					}
					
					invioJSON.put("qlfAndValue", metrics);
					
					//System.out.println(invioJSON.toJSONString());
					ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, invioJSON.toJSONString());
					producer.send(record);
					
					
				}
				Thread.sleep(1000);
			}
			producer.close();
			
			
		} catch (Exception e) {
			StringWriter errs = new StringWriter();
			e.printStackTrace(new PrintWriter(errs));
			loggo.info(errs.toString());
			
		}
	}

}

