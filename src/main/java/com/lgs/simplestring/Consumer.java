/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lgs.simplestring;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;

/**
 *
 * @author matteo
 */
public class Consumer {

	final static Logger loggo = Logger.getLogger(Consumer.class);
	
	public static void main(String[] args) {
		
		try {
		
			// TODO Auto-generated method stub
			Properties props = new Properties();
			//props.put("bootstrap.servers","10.0.2.50:9092");
			props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"10.0.2.50:9092");
			props.put(ConsumerConfig.GROUP_ID_CONFIG, "mygroup");
			//props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			//props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			
			KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
			consumer.subscribe(Arrays.asList("mytopic1"));
			
			boolean running = true;
			while (running) {
				ConsumerRecords<String, String> rcds = consumer.poll(100);
				for (ConsumerRecord<String, String> record : rcds){
					System.out.println(record.value());
				}
			}
			consumer.close();
			
			TimeUnit.SECONDS.sleep(1);
			loggo.info("==========DONE!!========");
			
		} catch (Exception e) {
			loggo.info("errore: " + e.getMessage());
		}
	}

}

